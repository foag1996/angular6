import { Component, EventEmitter, Output, OnInit, forwardRef, Inject } from '@angular/core';
import { DestinoViaje } from './../../model/destino-viaje.model';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>; //evento//
  fg:FormGroup; //se inicializa el formulario//
  minLongitud = 3; //nombre 3 caracterres//
  searchResults:string[]; //se define searchresults//

  constructor(fb: FormBuilder,  @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter(); //evento//
    this.fg = fb.group({  //se inicializa//
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])], //controles y validador//
      url: ['']
    });

    this.fg.valueChanges.subscribe( //registra un obsercable//
      (form: any) => {
        console.log('cambio en el formulario:', form);
      }
    );
  
  

   }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre'); //mostrar sugerencias la moento de escribir en nombre //
    fromEvent(elemNombre, 'input')//escucha cada vez que el usuario toca una tecla en la caja de texto//
    .pipe( //evento observable //
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),//mira el string que se escribe y muestra sugerencia//
      filter(text => text.length > 2),//filtra//
		  debounceTime(200),//stop de tiempo de tecleado de letras//
      distinctUntilChanged(),//llega la variable//
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre:string, url:string):boolean {
  	let d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } { //vlida que el nombre tenga mas de 5 caracteres//
    let l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return {invalidNombre: true}; //logica//
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
    let l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
            return { 'minLongNombre': true }; //logica//
        }
        return null;
    };
}

}
