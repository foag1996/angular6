import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje} from './../../model/destino-viaje.model';
import { DestinosApiClient } from './../../model/destinos-api-client.model';
import { Store, State } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../model/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates:string[]; //update//
  

  constructor(private destinosApiClient:DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = []; // se inicializa update//
    
  }

  ngOnInit(): void {  
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if (d != null) {
        this.updates.push("Se eligió: " + d.nombre);
      }
    });     
  }

  agregado(d:DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
   
  }

   //se crea elegido  y se marca como seleccionado//
  elegido(d:DestinoViaje) {

  //se desmarca el resto//
     this.destinosApiClient.elegir(d);
     
  }

  getAll() {

  }

}
